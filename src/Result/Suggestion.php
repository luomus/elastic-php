<?php

namespace Elastic\Result;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

class Suggestion implements IteratorAggregate
{

    protected $suggests = [];

    public function initialize($suggests)
    {
        $this->suggests = $suggests;
    }

    public function getNames()
    {
        return array_keys($this->suggests);
    }

    public function getIteratorFor($name)
    {
        if (!isset($this->suggests[$name]) || !isset($this->suggests[$name]) || !isset($this->suggests[$name][0]['options'])) {
            return new ArrayIterator([]);
        }
        return new ArrayIterator($this->suggests[$name][0]['options']);
    }

    /**
     * (PHP 5 >= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     *
     * @throws \Exception
     */
    public function getIterator()
    {
        return new ArrayIterator($this->suggests);
    }
}