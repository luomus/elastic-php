<?php

namespace Elastic\Result;


class TypeMapping
{

    const RAW_SUFFIX = '.raw';

    private $properties = [];
    private $flatted;

    public function __construct($mappings)
    {
        if (isset($mappings['properties'])) {
            $this->properties = $mappings['properties'];
        }
    }

    public function hasProperty($property)
    {
        return $this->getProperty($property) !== null;
    }

    public function hasRawProperty($property)
    {
        $property = $this->getProperty($property);
        if ($property === null) {
            return false;
        }
        return (
            isset($property['fields']) &&
            isset($property['fields']['raw'])
        );
    }

    public function getPropertyType($property)
    {
        $property = $this->getProperty($property);
        if ($property === null || !isset($property['type'])) {
            return null;
        }
        return $property['type'];
    }

    public function getProperty($property)
    {
        if (isset($this->properties[$property])) {
            return $this->properties[$property];
        }
        $flatted = $this->getFlattedProperty();
        return isset($flatted[$property]) ? $flatted[$property] : null;
    }

    public function getProperties($dataAsValue = false, $with = null)
    {
        $fields = [];
        if ($with !== null) {
            $with = str_replace('.', '', $with);
        }
        foreach ($this->properties as $field => $data) {
            if ($with !== null) {
                if (!isset($data['fields']) || !isset($data['fields'][$with])) {
                    continue;
                }
            }
            if ($dataAsValue) {
                $fields[$field] = $data;
            } else {
                $fields[$field] = $field;
            }
        }
        return $fields;
    }

    public function getFlattedPropertyTypes()
    {
        $flatted = $this->getFlattedProperty();
        $result = [];
        foreach ($flatted as $field => $data) {
            if (isset($data['type'])) {
                if ($data['type'] === 'date') {
                    $result[$field] = $data['format'] === 'date' ? 'date' : 'datetime';
                } else {
                    $result[$field] = $data['type'];
                }
            }
        }
        return $result;
    }

    public function getFlattedProperty()
    {
        if ($this->flatted === null) {
            $this->flatted = $this->flattenProperties($this->properties);
        }
        return $this->flatted;
    }

    private function flattenProperties($properties, $prefix = '', array $result = [])
    {
        foreach ($properties as $field => $data) {
            $field = $prefix . $field;
            if (isset($data['type'])) {
                $result[$field] = $data;
            } elseif (isset($data['properties'])) {
                $result[$field] = ['type' => 'object'];
                $result = array_merge($result, $this->flattenProperties($data['properties'], $field . '.'));
            }
        }
        return $result;
    }


}