<?php

namespace Elastic\Result;

use ArrayIterator;
use IteratorAggregate;
use Traversable;

class Aggregation implements IteratorAggregate
{

    protected $aggr = [];

    public function initialize($aggregators)
    {
        $this->aggr = $aggregators;
    }

    public function getNames()
    {
        return array_keys($this->aggr);
    }

    public function getIteratorFor($name)
    {
        if (!isset($this->aggr[$name]) || !isset($this->aggr[$name]['buckets'])) {
            return new ArrayIterator([]);
        }
        return new ArrayIterator($this->aggr[$name]['buckets']);
    }

    /**
     * (PHP 5 >= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     * <b>Traversable</b>
     *
     * @throws \Exception
     */
    public function getIterator()
    {
        return new ArrayIterator($this->aggr);
    }
}