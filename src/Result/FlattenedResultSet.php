<?php

namespace Elastic\Result;

use Zend\Db\Exception;
use Zend\Db\ResultSet\ResultSet;

class FlattenedResultSet extends ResultSet
{
    protected $flattened = [];

    public function __construct(array $flattened = null, $arrayObjectPrototype = null)
    {
        if ($flattened !== null) {
            $this->setFlattened($flattened);
        }
        parent::__construct(self::TYPE_ARRAY, $arrayObjectPrototype);
    }

    /**
     * @return array
     */
    public function getFlattened()
    {
        return $this->flattened;
    }

    /**
     * @param array $flattened
     */
    public function setFlattened(array $flattened)
    {
        $flattened = array_filter($flattened, function ($v) {
            return strpos($v, '.') !== false;
        });
        foreach ($flattened as $flat) {
            $parts = explode('.', $flat);
            $this->flattened[$flat] = $this->assocArray($parts);
        }
    }

    private function assocArray($fields, $result = [])
    {
        $current = array_shift($fields);
        if ($current === null) {
            return '';
        }
        $result[$current] = $this->assocArray($fields, $result);
        return $result;
    }

    public function current()
    {
        $result = parent::current();
        foreach ($this->flattened as $flatted => $location) {
            $result[$flatted] = $this->getDeepValue($result, $location);
        }
        return $result;
    }

    protected function getDeepValue(array $array1, array $array2)
    {
        $array1 = array_intersect_key($array1, $array2);
        foreach ($array1 as $key => $value) {
            if (!is_array($array2[$key])) {
                return $value;
            } else if (is_array($value)) {
                return $this->getDeepValue($value, $array2[$key]);
            }
        }
        return null;
    }
}