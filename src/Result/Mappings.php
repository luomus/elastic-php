<?php

namespace Elastic\Result;


class Mappings
{

    private $types = [];

    public function initialize($result)
    {
        foreach ($result as $type => $mappings) {
            $this->types[$type] = new TypeMapping($mappings);
        }
    }

    public function getTypeMappings($type)
    {
        if (isset($this->types[$type])) {
            return $this->types[$type];
        }
        return new TypeMapping([]);
    }

}