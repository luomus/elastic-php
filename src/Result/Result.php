<?php
namespace Elastic\Result;


use Zend\Db\Adapter\Driver\ResultInterface;

class Result implements ResultInterface
{

    protected $result;
    protected $spot;
    protected $error;

    public function initialize($result)
    {
        $this->result = $result;
    }

    public function getError()
    {
        return $this->error;
    }

    public function setError($error)
    {
        $this->error = $error;
    }

    public function getSuggestions($name = null)
    {
        $suggest = new Suggestion();
        if (isset($this->result['suggest'])) {
            $suggest->initialize($this->result['suggest']);
        }
        if ($name !== null) {
            return $suggest->getIteratorFor($name);
        }
        return $suggest;
    }

    public function getAggregations($name = null)
    {
        $agg = new Aggregation();
        if (isset($this->result['aggregations'])) {
            $agg->initialize($this->result['aggregations']);
        }
        if ($name !== null) {
            return $agg->getIteratorFor($name);
        }
        return $agg;
    }

    public function getTook()
    {
        if (!isset($this->result['took'])) {
            return -1;
        }
        return $this->result['took'];
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the current element
     * @link http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     */
    public function current()
    {
        if (isset($this->result['hits']['hits'][$this->spot])) {
            return $this->result['hits']['hits'][$this->spot]['_source'];
        }
        return null;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Move forward to next element
     * @link http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     */
    public function next()
    {
        $this->spot++;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Return the key of the current element
     * @link http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     */
    public function key()
    {
        return $this->spot;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Checks if current position is valid
     * @link http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     */
    public function valid()
    {
        return isset($this->result['hits']['hits'][$this->spot]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Rewind the Iterator to the first element
     * @link http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     */
    public function rewind()
    {
        $this->spot = 0;
    }

    /**
     * Force buffering
     *
     * @return void
     */
    public function buffer()
    {

    }

    /**
     * Check if is buffered
     *
     * @return bool|null
     */
    public function isBuffered()
    {
        return null;
    }

    /**
     * Is query result?
     *
     * @return bool
     */
    public function isQueryResult()
    {
        return isset($this->query);
    }

    /**
     * Get affected rows
     *
     * @return int
     */
    public function getAffectedRows()
    {
        return null;
    }

    /**
     * Get generated value
     *
     * @return mixed|null
     */
    public function getGeneratedValue()
    {
        return null;
    }

    /**
     * Get the resource
     *
     * @return mixed
     */
    public function getResource()
    {
        return $this->result;
    }

    /**
     * Get field count
     *
     * @return int
     */
    public function getFieldCount()
    {
        // TODO: Implement getFieldCount() method.
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return isset($this->result['hits']['total']) ? $this->result['hits']['total'] : 0;
    }
}