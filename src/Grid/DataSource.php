<?php

namespace Elastic\Grid;


use Elastic\Client\Search;
use Elastic\Paginator\Adapter\Elastic as ElasticPaginator;
use Elastic\Query\Query;
use Elastic\Query\Sort\SortByDistance;
use Elastic\Result\FlattenedResultSet;
use Elastic\Result\TypeMapping;
use Kotka\DataGrid\Type\GeoPointType;
use ZfcDatagrid\Column\Select;
use ZfcDatagrid\Column\Type;
use ZfcDatagrid\DataSource\AbstractDataSource;

class DataSource extends AbstractDataSource
{

    protected $query;

    protected $select;

    /** @var TypeMapping */
    protected $mappings;

    /**
     *
     * Set the data source
     * - array
     * - ZF2: Zend\Db\Sql\Select
     * - Doctrine2: Doctrine\ORM\QueryBuilder
     * - ...
     *
     * @param Query $data
     * @param Search $select
     * @param TypeMapping $mappings
     * @throws \Exception
     */
    public function __construct($data, Search $select = null, TypeMapping $mappings = null)
    {
        if (!$data instanceof Query) {
            throw new \Exception("First parameter in elastic datasource should be Query");
        }
        if ($select === null) {
            throw new \Exception("Missing second parameter from elastic datasource");
        }
        $this->query = clone $data;
        $this->select = $select;
        $this->mappings = $mappings;
    }

    /**
     * Get the data back from construct
     *
     * @return Query
     */
    public function getData()
    {
        return $this->query;
    }

    /**
     * Execute the query and set the paginator
     * - with sort statements
     * - with filters statements
     */
    public function execute()
    {
        $query = $this->getData();

        /*
         * Step 1) Apply needed columns
         */
        $selectColumns = array();
        foreach ($this->getColumns() as $column) {
            if ($column instanceof Select) {
                $selectColumns[] = $column->getUniqueId();
            }
        }
        $query->setFields($selectColumns);

        /*
         * Step 2) Apply sorting
         */
        $conditions = $this->getSortConditions();
        if (count($conditions) > 0) {
            // Minimum one sort condition given -> so reset the default orderBy
            $query->clearSort();

            foreach ($conditions as $key => $sortCondition) {
                /* @var $column \ZfcDatagrid\Column\AbstractColumn */
                $column = $sortCondition['column'];
                $direction = $sortCondition['sortDirection'] === 'ASC' ? 'asc' : 'desc';
                $col = $this->getSortByColumn($column->getUniqueId());
                $type = $column->getType();
                if ($type instanceof GeoPointType) {
                    $lat = $type->getCenterLat();
                    $lon = $type->getCenterLon();
                    if ($lat !== null && $lon !== null) {
                        $sort = new SortByDistance($col, $direction, $lat, $lon);
                        $query->addSort($col, $sort);
                    } else {
                        $column->setSortActive(null);
                    }
                } else {
                    $query->addSort($col, $direction);
                }
            }
            $this->setSortConditions($conditions);
        }

        /*
         * Step 3) Apply filters
         */
        // TODO: add filters to query (we're using external filters ATM so there is no rush with this one)

        /*
         * Step 4) Pagination
         */
        $this->setPaginatorAdapter(new ElasticPaginator($query, $this->select, new FlattenedResultSet($selectColumns)));
    }

    protected function getSortByColumn($column)
    {
        $type = $this->mappings->getPropertyType($column);
        if ($type === 'text' && $this->mappings->hasRawProperty($column)) {
            $column = $column . TypeMapping::RAW_SUFFIX;
        }

        return $column;
    }
}