<?php
namespace Elastic\Query;

class Aggregate
{

    const TYPE_TERMS = 'terms';
    const TYPE_HISTOGRAM = 'histogram';
    const TYPE_DATE_HISTOGRAM = 'date_histogram';

    /** @var string */
    protected $name;
    /** @var string */
    protected $field;
    /** @var string */
    protected $type;
    /** @var int */
    protected $size;
    /** @var int */
    protected $min_doc_count;
    /** @var string */
    protected $interval;
    /** @var string */
    protected $format;

    protected $aggregates = [];

    protected $filters = [];

    public function __construct($type = null, $field = null, $name = null, $size = null, $interval = null)
    {
        if ($type !== null) {
            $this->setType($type);
        }
        if ($name !== null) {
            $this->setName($name);
        }
        if ($size !== null) {
            $this->setSize($size);
        }
        if ($field !== null) {
            $this->setField($field);
        }
        if ($interval !== null) {
            $this->setInterval($interval);
        }
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param string $interval
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;
    }

    /**
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return int
     */
    public function getMinDocCount()
    {
        return $this->min_doc_count;
    }

    /**
     * @param int $min_doc_count
     */
    public function setMinDocCount($min_doc_count)
    {
        $this->min_doc_count = (int)$min_doc_count;
    }

    public function addAggregate(Aggregate $aggregate)
    {
        $this->aggregates[] = $aggregate;
    }

    public function clearAggregate()
    {
        $this->aggregates = [];
    }

    public function toArray()
    {
        $type = $this->type;
        $result[$type]['field'] = $this->field;
        if ($this->size !== null) {
            $result[$type]['size'] = $this->size;
        }
        if ($this->min_doc_count !== null) {
            $result[$type]['min_doc_count'] = $this->min_doc_count;
        }
        if ($this->interval !== null) {
            $result[$type]['interval'] = $this->interval;
        }
        if (!empty($this->sort)) {
            foreach ($this->sort as $field => $order) {
                $result[$type]['order'] = [
                    $field => $order
                ];
            }
        }
        if (!empty($this->aggregates)) {
            foreach ($this->aggregates as $aggregate) {
                /** @var Aggregate $aggregate */
                $result['aggs'][$aggregate->getName()] = $aggregate->toArray();
            }
        }
        return $result;
    }
}