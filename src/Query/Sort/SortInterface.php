<?php

namespace Elastic\Query\Sort;


interface SortInterface
{
    public function toArray();
}