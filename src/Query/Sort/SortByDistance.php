<?php

namespace Elastic\Query\Sort;


class SortByDistance extends AbstractSort
{

    /** @var  string */
    protected $order;
    /** @var  float */
    protected $lat;
    /** @var  float */
    protected $lon;
    /** @var string */
    protected $unit = 'km';

    public function __construct($field = null, $order = 'asc', $lat = 0, $lon = 0)
    {
        parent::__construct($field);
        $this->setOrder($order);
        $this->setLon($lon);
        $this->setLat($lat);
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat($lat)
    {
        $this->lat = floatval($lat);
    }

    /**
     * @return float
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * @param float $lon
     */
    public function setLon($lon)
    {
        $this->lon = floatval($lon);
    }

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    public function toArray()
    {
        return [
            '_geo_distance' => [
                $this->field => [
                    'lat' => $this->lat,
                    'lon' => $this->lon
                ],
                'order' => $this->order,
                'unit' => $this->unit,
                'distance_type' => "plane"
            ]
        ];
    }


}