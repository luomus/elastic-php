<?php
namespace Elastic\Query\Sort;


abstract class AbstractSort implements SortInterface
{
    /** @var string */
    protected $field;

    public function __construct($field = null)
    {
        $this->setField($field);
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }
}