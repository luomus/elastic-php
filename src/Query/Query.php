<?php

namespace Elastic\Query;


use Elastic\Query\Filter\Filter;
use Elastic\Query\Sort\SortInterface;

class Query
{

    const CARDINALITY_ACCURACY = 10000;

    const SORT_ASC = 'asc';
    const SORT_DESC = 'desc';

    /** @var string wildcard query */
    protected $wildcard = ['query' => '', 'field' => '_all'];
    /** @var string query string */
    protected $queryString = '';
    /** @var array of sorting */
    protected $sort = [];
    /** @var int # of first item */
    protected $from = 0;
    /** @var int page size */
    protected $size;
    /** @var array fields that should be returned in the result */
    protected $fields = [];

    /** @var string index */
    protected $index;
    /** @var  string type */
    protected $type;

    protected $aggregates = [];

    /** @var Suggest|null */
    protected $suggest;

    protected $filter = [];

    public function __construct($index = null, $type = null)
    {
        $this->setIndex($index);
        $this->setType($type);
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        return $this->queryString;
    }

    /**
     * @param $queryString
     * @return $this
     */
    public function setQueryString($queryString)
    {
        $this->queryString = $queryString;

        return $this;
    }

    /**
     * @return string
     */
    public function getWildcard()
    {
        return $this->wildcard;
    }

    /**
     * @param $wildcard
     * @param string|null $field
     */
    public function setWildcard($wildcard, $field = null)
    {
        $this->wildcard['query'] = $wildcard;
        if ($field !== null) {
            $this->wildcard['field'] = $field;
        }
    }

    /**
     * @return Suggest|null
     */
    public function getSuggest()
    {
        return $this->suggest;
    }

    /**
     * @param Suggest $suggest
     */
    public function setSuggest(Suggest $suggest)
    {
        $this->suggest = $suggest;
    }

    public function clearSuggest()
    {
        $this->suggest = null;
    }

    /**
     * @return array
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param array $sort
     * @return $this
     */
    public function setSort(array $sort)
    {
        $this->sort = $sort;

        return $this;
    }

    public function addSort($field, $order = self::SORT_DESC)
    {
        $this->sort[$field] = $order;

        return $this;
    }

    public function clearSort()
    {
        $this->sort = [];

        return $this;
    }

    /**
     * @return int
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param int $from
     * @return $this
     */
    public function setFrom($from)
    {
        $this->from = (int)$from;

        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function setSize($size)
    {
        $this->size = (int)$size;

        return $this;
    }

    /**
     * @return string
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param string $index
     * @return $this
     */
    public function setIndex($index)
    {
        $this->index = $index;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function addFilter(Filter $filter)
    {
        $this->filter[] = $filter;
    }

    public function clearFilter()
    {
        $this->filter = [];
    }

    public function addAggregate(Aggregate $aggregate)
    {
        $this->aggregates[] = $aggregate;

        return $this;
    }

    public function clearAggregate()
    {
        $this->aggregates = [];
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    public function toArray()
    {
        $params = [];
        $params['index'] = $this->index;
        if ($this->type !== null) {
            $params['type'] = $this->type;
        }
        if ($this->size !== null) {
            $params['size'] = $this->size;
        }
        if ($this->from > 1) {
            $params['from'] = $this->from;
        }
        if (!empty($this->filter)) {
            foreach ($this->filter as $filter) {
                /** @var Filter $filter */
                $params['body']['query']['bool']['filter'][$filter->getType()] = $filter->toArray();
            }
        }
        if (!empty($this->fields)) {
            $params['body']['_source'] = $this->fields;
        }
        if (!empty($this->queryString)) {
            $params['body']['query']['bool']['must']['query_string']['query'] = $this->queryString;
        }
        if (!empty($this->wildcard['query'])) {
            $params['body']['query']['bool']['filter']['wildcard'][$this->wildcard['field']] = $this->wildcard['query'];
        }
        if (!empty($this->sort)) {
            foreach ($this->sort as $field => $order) {
                if ($order instanceof SortInterface) {
                    $params['body']['sort'][] = $order->toArray();
                } else {
                    $params['body']['sort'][] = [
                        $field => [
                            'order' => $order
                        ]
                    ];
                }

            }
        }
        if (!empty($this->aggregates)) {
            foreach ($this->aggregates as $aggregate) {
                /** @var Aggregate $aggregate */
                $params['body']['aggs'][$aggregate->getName()] = $aggregate->toArray();
                $params['body']['aggs'][$aggregate->getName() . '_count'] = ['cardinality' => [
                    'field' => $aggregate->getField(),
                    'precision_threshold' => Query::CARDINALITY_ACCURACY
                ]];
            }
        }
        $suggest = $this->suggest;
        if ($suggest instanceof Suggest) {
            $params['body']['_source'] = $suggest->getField() . '.output';
            $params['body']['suggest'] = [
                $suggest->getField() => [
                    'text' => $suggest->getText(),
                    'completion' => [
                        'field' => $suggest->getField() . '.auto',
                        'size' => $suggest->getSize(),
                    ]
                ]
            ];
        }
        return $params;
    }

}