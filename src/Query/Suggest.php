<?php

namespace Elastic\Query;


class Suggest
{

    const MODE_MISSING = 'missing';
    const MODE_POPULAR = 'popular';
    const MODE_ALWAYS = 'always';

    const TYPE_TERM = 'term';

    protected $field;
    protected $mode = self::MODE_MISSING;
    protected $size = 10;
    protected $text;
    protected $type = self::TYPE_TERM;

    protected $modes = [
        self::MODE_ALWAYS,
        self::MODE_POPULAR,
        self::MODE_MISSING,
    ];

    protected $types = [
        self::TYPE_TERM
    ];

    public function __construct($field = null, $text = null, $size = 10)
    {
        $this->field = $field;
        $this->text = $text;
        $this->size = $size;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     * @throws \Exception
     */
    public function setMode($mode)
    {
        if (!in_array($mode, $this->modes)) {
            throw new \Exception('Invalid mode ' . $mode . ' given to Suggest');
        }
        $this->mode = $mode;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize($size)
    {
        $this->size = (int)$size;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @throws \Exception
     */
    public function setType($type)
    {
        if (!in_array($type, $this->modes)) {
            throw new \Exception('Invalid type ' . $type . ' given to Suggest');
        }
        $this->type = $type;
    }

}