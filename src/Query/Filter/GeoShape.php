<?php

namespace Elastic\Query\Filter;


class GeoShape extends Filter
{
    protected $type = 'geo_shape';

    protected $shapeType;
    protected $coordinates;

    public function __construct($field = null, $shapeType = 'Point', $coordinates = [])
    {
        parent::__construct($field);
        $this->setShapeType($shapeType);
        $this->setCoordinates($coordinates);
    }

    /**
     * @return string
     */
    public function getShapeType()
    {
        return $this->shapeType;
    }

    /**
     * @param string $shapeType
     */
    public function setShapeType($shapeType)
    {
        $this->shapeType = $shapeType;
    }

    /**
     * @return array
     */
    public function getCoordinates()
    {
        return $this->coordinates;
    }

    /**
     * @param array $coordinates
     */
    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;
    }

    public function toArray()
    {
        return [
            $this->field => [
                'shape' => [
                    'type' => $this->shapeType,
                    'coordinates' => $this->coordinates
                ]
            ]
        ];
    }
}