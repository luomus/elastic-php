<?php

namespace Elastic\Query\Filter;


Abstract class Filter
{
    /** @var string */
    protected $type;
    /** @var string */
    protected $field;

    public function __construct($field = null)
    {
        $this->setField($field);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     */
    public function setField($field)
    {
        $this->field = $field;
    }

    public function toArray()
    {
        return ['field' => $this->field];
    }
}