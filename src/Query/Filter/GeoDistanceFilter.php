<?php

namespace Elastic\Query\Filter;


class GeoDistanceFilter extends Filter
{
    protected $type = 'geo_distance';
    /** @var float */
    protected $centerLat;
    /** @var float */
    protected $centerLon;
    protected $distance;

    public function __construct($field = null, $lat = null, $lon = null, $dist = null)
    {
        parent::__construct($field);
        $this->setCenterLat($lat);
        $this->setCenterLon($lon);
        $this->setDistance($dist);
    }

    /**
     * @return float
     */
    public function getCenterLat()
    {
        return $this->centerLat;
    }

    /**
     * @param float $centerLat
     */
    public function setCenterLat($centerLat)
    {
        $this->centerLat = $centerLat;
    }

    /**
     * @return float
     */
    public function getCenterLon()
    {
        return $this->centerLon;
    }

    /**
     * @param float $centerLon
     */
    public function setCenterLon($centerLon)
    {
        $this->centerLon = $centerLon;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param mixed $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    public function toArray()
    {
        return [
            'distance' => $this->getDistance(),
            'wgs84Location' => [
                'lat' => $this->getCenterLat(),
                'lon' => $this->getCenterLon()
            ]
        ];
    }

}
