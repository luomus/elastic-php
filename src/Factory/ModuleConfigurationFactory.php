<?php

namespace Elastic\Factory;

use Elastic\Options\ModuleConfiguration;
use RuntimeException;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class ModuleConfigurationFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $options = $this->getOptions($serviceLocator);
        $config = new ModuleConfiguration($options);

        return $config;
    }

    /**
     * @param  ServiceLocatorInterface $serviceLocator
     * @return mixed
     * @throws RuntimeException
     */
    public function getOptions(ServiceLocatorInterface $serviceLocator)
    {
        $options = $serviceLocator->get('Config');
        $options = isset($options['elastic']) ? $options['elastic'] : null;

        if (null === $options) {
            throw new RuntimeException(
                'Configuration for elastic module could not be found in "elastic".'
            );
        }

        return $options;
    }
}
