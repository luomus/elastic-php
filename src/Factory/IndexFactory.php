<?php

namespace Elastic\Factory;

use Elastic\Client\Index;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class IndexFactory implements FactoryInterface
{

    /**
     * Create service
     *
     * @param ServiceLocatorInterface $serviceLocator
     *
     * @return mixed
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        /** @var \Elastic\Options\ModuleConfiguration $configurations */
        $configurations = $serviceLocator->get('Elastic\Options\ModuleConfiguration');

        return new Index($configurations->getClient());
    }
}