<?php
namespace Elastic\Options;

use Zend\Stdlib\AbstractOptions;

class ModuleConfiguration extends AbstractOptions
{

    protected $client;

    /**
     * @return array
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param array $client
     */
    public function setClient(array $client)
    {
        $this->client = $client;
    }


} 