<?php

namespace Elastic\Service;


use Elastic\Extract\Autocomplete;
use Elastic\Extract\ExtractInterface;
use Elastic\Query\Query;
use Elastic\Query\Suggest;
use Kotka\Service\DropDownService;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;

class AutocompleteService implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    protected $adapter;

    protected $fields;

    protected $extract;

    public function autocomplete($field, $search, $size = 10, array $searchFromFields = ['_missing_', '_exists_'])
    {
        if (empty($search) || !$this->hasAutocomplete($field)) {
            if ($this->extract !== null  && in_array($field, $searchFromFields, $size)) {
                return $this->fieldAutocomplete($search, $size, $this->extract);
            }
            return [];
        }
        return $this->valueAutocomplete($field, $search, $size);
    }

    protected function valueAutocomplete($field, $search, $size) {
        $extract = $this->getExtract();
        $query = new Query(Autocomplete::getIndexName(), Autocomplete::getTypeName($extract));
        $adapter = $this->getAdapter();
        $query->setSize(0);
        $query->setSuggest(new Suggest($field, $search, $size));
        $results = $adapter->query($query)->execute()->getSuggestions($field);
        $return = [];
        foreach ($results as $result) {
            if (!isset($result['_source']) || count($result['_source']) === 0) {
                continue;
            }
            $return[] = array_pop($result['_source'])['output'];
        }
        return $return;
    }

    protected function fieldAutocomplete($search, $size, ExtractInterface $extract) {
        $adapter = $this->getAdapter();
        $fields  = $adapter->getMappings($extract::getIndexName(), $extract::getTypeName())->getProperties();
        $search  = mb_strtolower($search, 'utf-8');
        $results = [];
        $cnt     = 0;
        if (empty($search)) {
            return array_slice(array_values($fields), 0, $size);
        }
        foreach ($fields as $key => $value) {
            $lowerValue = mb_strtolower($value, 'utf-8');
            if (strpos($lowerValue, $search) === 0 || strpos($lowerValue, ' '. $search) !== false) {
                $results[] = $value;
                $cnt++;
                if ($cnt > $size) {
                    break;
                }
            }
        }
        return $results;
    }

    public function hasAutocomplete($field)
    {
        return $this->getFields()->hasProperty($field);
    }

    /**
     * @return \Elastic\Result\TypeMapping
     * @throws \Exception
     */
    public function getFields()
    {
        if ($this->fields === null) {
            $extract = $this->getExtract();
            if ($extract === null) {
                throw new \Exception('You need to set extractor to AutocompleteService before using autocomplete.');
            }
            $this->fields = $this->getAdapter()->getMappings(Autocomplete::getIndexName(), Autocomplete::getTypeName($extract));
        }
        return $this->fields;
    }

    /**
     * @return ExtractInterface
     */
    public function getExtract()
    {
        return $this->extract;
    }

    /**
     * @param ExtractInterface $extract
     */
    public function setExtract(ExtractInterface $extract)
    {
        $this->fields = null;
        $this->extract = $extract;
    }

    /**
     * @return \Elastic\Client\Search
     */
    protected function getAdapter()
    {
        if ($this->adapter === null) {
            $this->adapter = $this->serviceLocator->get('Elastic\Client\Search');
        }
        return $this->adapter;
    }
}