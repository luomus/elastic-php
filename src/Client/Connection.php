<?php
namespace Elastic\Client;

use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class Connection
{

    /** @var Client */
    private $client;
    private $params;

    public function __construct($params)
    {
        $this->params = $params;
    }

    public function getClient()
    {
        if ($this->client === null) {
            $hosts = isset($this->params['hosts']) ? $this->params['hosts'] : ['localhost'];
            $this->client = ClientBuilder::create()->fromConfig($this->params);
        }
        return $this->client;
    }

}