<?php

namespace Elastic\Client;

use Elastic\Extract\ExtractInterface;
use Elastic\Query\Query;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerAwareTrait;

/**
 * Class Index is responsible of handling indexes on the elasticsearch server
 *
 * @package Elastic\Client
 */
class Index extends Connection implements EventManagerAwareInterface
{

    use EventManagerAwareTrait;

    const SHARDS = 5;
    const REPLICAS = 0;

    /**
     * Holds the "index_type" keys so that the index is checked only once
     * @var array
     */
    private $checked = [];

    /**
     * Indexes single object to index
     *
     * @param mixed $object
     * @param ExtractInterface $extractor
     * @return array
     */
    public function indexItem($object, ExtractInterface $extractor)
    {
        $this->prepareIndex($extractor);
        $bulk = [];
        $extractor->addToBulk($object, $bulk);

        return $this->getClient()->bulk($bulk);
    }


    /**
     *
     * * Indexes the with result set using bulk insert
     *
     * @param array|\Traversable $resultSet
     * @param ExtractInterface $extractor
     * @param int $batchSize
     * @throws \Exception
     */
    public function indexResults($resultSet, ExtractInterface $extractor, $batchSize = 100000)
    {
        if (!is_array($resultSet) && !$resultSet instanceof \Traversable) {
            throw new \Exception('Invalid resultset give to index results. Resultsset should be Traversable');
        }
        $this->prepareIndex($extractor);
        $bulk = [];
        $cnt = 0;
        foreach ($resultSet as $result) {
            $cnt = isset($bulk['body']) ? count($bulk['body']) / 2 : 0;
            $extractor->addToBulk($result, $bulk);
            if ($cnt > $batchSize) {
                $this->getEventManager()->trigger('elastic', $this, ['spot' => $cnt, 'action' => 'sending']);
                $this->getClient()->bulk($bulk);
                $this->getEventManager()->trigger('elastic', $this, ['action' => 'sent']);
                $bulk = [];
            }
            $this->getEventManager()->trigger('indexed', $this, ['spot' => $cnt]);
        }
        if (count($bulk)) {
            $this->getEventManager()->trigger('elastic', $this, ['spot' => $cnt, 'action' => 'sending']);
            $this->getClient()->bulk($bulk);
            $this->getEventManager()->trigger('elastic', $this, ['action' => 'sent']);
            $this->getEventManager()->trigger('indexed', $this, ['spot' => $cnt, 'finnish' => true]);
        }
    }

    public function deleteByQuery(Query $query)
    {
        return $this->getClient()->deleteByQuery($query->toArray());
    }

    /**
     * Deletes the index
     *
     * @param $index
     * @param null $type
     */
    public function delete($index, $type = null)
    {
        $delete['index'] = $index;
        if ($this->hasIndex($index)) {
            $this->getClient()->indices()->delete($delete);

        }
        if ($type !== null && $this->hasType($index, $type)) {
            $delete['type'] = $type;
            $this->getClient()->indices()->deleteMapping($delete);
        }
    }

    /**
     * Checks and setups the index as needed
     *
     * @param ExtractInterface $extractor
     */
    private function prepareIndex(ExtractInterface $extractor)
    {
        $index = $extractor->getIndex();
        $type = $extractor::getTypeName();
        $checkedKey = $index . '_' . $type;
        if (isset($this->checked[$checkedKey])) {
            return;
        }
        if (!$this->hasType($index, $type)) {
            if (!$this->hasIndex($index)) {
                $this->createIndex($extractor);
            }
            $this->addMapping($extractor);
        }
        $this->checked[$checkedKey] = true;
        $related = $extractor->getRelatedExtracts();
        foreach ($related as $relate) {
            $this->prepareIndex($relate);
        }
    }

    /**
     * Checks to see if the index exists
     *
     * @param string $index
     * @return bool
     */
    private function hasIndex($index)
    {
        return $this->getClient()->indices()->exists(['index' => $index]);
    }

    /**
     * Checks to see if the type exists in the index
     *
     * @param string $index
     * @param string $type
     * @return array
     */
    private function hasType($index, $type)
    {
        return $this->getClient()->indices()->existsType(['index' => $index, 'type' => $type]);
    }

    /**
     * Adds type mapping to index
     *
     * @param ExtractInterface $extractor
     */
    private function addMapping(ExtractInterface $extractor)
    {
        $type = $extractor::getTypeName();
        $params['index'] = $extractor->getIndex();
        $params['type'] = $type;
        $params['body'][$type] = $extractor->getMapping();
        $this->getClient()->indices()->putMapping($params);
    }

    /**
     * Creates the index
     *
     * @param $extractor
     * @param int $shards
     * @param int $replicas
     */
    private function createIndex(ExtractInterface $extractor, $shards = self::SHARDS, $replicas = self::REPLICAS)
    {
        $params = [
            'index' => $extractor->getIndex(),
            'body' => [
                'settings' => $extractor->getSettings()
            ]
        ];
        $this->getClient()->indices()->create($params);
    }


}