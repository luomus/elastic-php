<?php

namespace Elastic\Client;

use Elastic\Query\Query;
use Elastic\Result\Mappings;
use Elastic\Result\Result;
use Elastic\Result\TypeMapping;

class Search extends Connection
{

    /** @var Query */
    protected $query;
    protected $mappings = [];

    protected $resultPrototype;

    /**
     * @param Result $resultPrototype
     */
    public function setResultPrototype(Result $resultPrototype)
    {
        $this->resultPrototype = $resultPrototype;
    }

    /**
     * @return Result
     */
    public function getResultPrototype()
    {
        return $this->resultPrototype ?: new Result();
    }

    /**
     * @param $index
     * @return Mappings
     */
    protected function _getMappings($index)
    {
        if (!isset($this->mappings[$index])) {
            $params = ['index' => $index];
            $mappings = new Mappings();
            try {
                $result = $this->getClient()->indices()->getMapping($params);
            } catch (\Exception $e) {
                return $mappings;
            }
            if (isset($result[$index]) && isset($result[$index]['mappings'])) {
                $mappings->initialize($result[$index]['mappings']);
            }
            $this->mappings[$index] = $mappings;
        }
        return $this->mappings[$index];
    }

    /**
     * @param $index
     * @param null $type
     * @return Mappings|TypeMapping
     */
    public function getMappings($index, $type = null)
    {
        if (empty($index)) {
            return [];
        }
        $mappings = $this->_getMappings($index);
        if ($type !== null) {
            return $mappings->getTypeMappings($type);
        }
        return $mappings;
    }

    public function query(Query $query)
    {
        $this->query = $query;

        return $this;
    }

    public function suggest(Query $query)
    {
        $result = clone $this->getResultPrototype();
        try {
            $result->initialize(
                $this->getClient()->suggest($query->toArray())
            );
        } catch (\Exception $e) {
        }
        return $result;
    }


    public function execute()
    {
        if (!$this->query instanceof Query) {
            throw new \Exception("You forgot the add Query");
        }
        $result = clone $this->getResultPrototype();
        try {
            $result->initialize(
                $this->getClient()->search($this->query->toArray())
            );
        } catch (\Exception $e) {
            $result->setError($e->getMessage());
        }


        return $result;
    }
}