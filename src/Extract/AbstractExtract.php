<?php

namespace Elastic\Extract;


use Elastic\Client\Index;

abstract class AbstractExtract implements ExtractInterface
{
    protected static $type = 'default';
    protected static $index = 'default';

    public function init()
    {

    }

    public function addToBulk($data, &$bulk)
    {
        $this->prepareData($data);
        $bulk['body'][] = [
            'index' => [
                '_index' => self::getIndexName(),
                '_type' => self::getTypeName(),
            ]
        ];
        $bulk['body'][] = $data;
    }

    public function getSettings()
    {
        return [
            'number_of_shards' => Index::SHARDS,
            'number_of_replicas' => Index::REPLICAS,
        ];
    }

    /**
     * Returns the extracts that are related to this Extract.
     *
     * @return array
     */
    public function getRelatedExtracts()
    {
        return [];
    }

    public function getIndex()
    {
        return static::getIndexName();
    }

    public static function getTypeName()
    {
        return static::$type;
    }

    public static function getIndexName()
    {
        return static::$index;
    }

    protected function prepareData(&$data) {
        return;
    }

}