<?php

namespace Elastic\Extract;


interface ExtractInterface
{


    public function addToBulk($object, &$bulk);

    public function getMapping();

    public function getSettings();

    /**
     * Returns the extracts that are related to this Extract.
     *
     * @return array
     */
    public function getRelatedExtracts();

    /**
     * @return string
     */
    public function getIndex();

    public static function getTypeName();

    /**
     * @deprecated use instance method instead
     */
    public static function getIndexName();

}