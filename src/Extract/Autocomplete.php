<?php

namespace Elastic\Extract;

use Elastic\Client\Index;

class Autocomplete implements ExtractInterface
{
    const INDEX = 'autocomplete';
    const GENERIC_TYPE = '_default_';
    const MIN_INDEX_LEN = 3;

    private $suggestions = [];

    public function getMapping()
    {
        return [
            "dynamic_templates" => [
                [
                    "generic_autocomplete" => [
                        "path_match" => "*.auto",
                        "mapping" => [
                            'type' => 'completion',
                            'analyzer' => 'simple',
                            'search_analyzer' => 'simple',
                            //'preserve_position_increments' => false,
                            //'preserve_separators' => false
                        ]
                    ],
                ],
                [
                    "autocomplete_field" => [
                        "path_match" => "*.field",
                        "mapping" => [
                            'type' => 'keyword'
                        ]
                    ]
                ],
                [
                    "autocomplete_output" => [
                        "path_match" => "*.output",
                        "mapping" => [
                            'type' => 'keyword'
                        ]
                    ]
                ],
            ]
        ];
    }

    public function addToBulk($document, &$bulk, $type = '', array $fields = [])
    {
        if (empty($document) || !is_array($document)) {
            return;
        }
        $this->suggestions = [];
        foreach ($fields as $field) {
            if (isset($document[$field])) {
                $this->getInputSuggestion($document[$field], $field);
            }
        }
        foreach ($this->suggestions as $suggestion) {
            foreach ($suggestion as $field => $value) {
                $bulk['body'][] = [
                    'index' => [
                        '_index' => self::getIndexName(),
                        '_type' => $type,
                        '_id' => $field . ':' . $value['output']
                    ]
                ];
                $bulk['body'][] = [$field => $value];
            }
        }
        $this->suggestions = [];

    }

    private function getInputSuggestion($value, $field, $key = 0)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $this->getInputSuggestion($v, $field, $k);
            }
            return;
        }
        if (strlen($value) > 256) {
            return;
        }
        $suggestion = [];
        $val = $value;
        $parts = 0;
        do {
            $pos = strpos($val, ' ');
            if ($pos !== false) {
                if ($pos < self::MIN_INDEX_LEN) {
                    $val = substr($val, $pos + 1);
                    continue;
                }
                $suggestion[] = $val;
                $val = substr($val, $pos + 1);
            } else {
                if (strlen($val) < self::MIN_INDEX_LEN) {
                    break;
                }
                $suggestion[] = $val;
            }
            $parts++;
        } while ($pos !== false && $parts < 10);
        if (count($suggestion)) {
            $this->suggestions[$key][$field] = [
                'field' => $field,
                'output' => $value,
                'auto' => ['input' => $suggestion]
            ];
        }
    }

    public function getIndex()
    {
        return self::getIndexName();
    }

    public static function getTypeName(ExtractInterface $extract = null)
    {
        if ($extract === null) {
            return self::GENERIC_TYPE;
        }
        return $extract::getIndexName() . '_' . $extract::getTypeName();
    }

    public static function getIndexName()
    {
        return self::INDEX;
    }

    public function init()
    {
    }

    public function getSettings()
    {
        return [
            'number_of_shards' => Index::SHARDS,
            'number_of_replicas' => Index::REPLICAS,
        ];
    }

    public function getRelatedExtracts()
    {
        return [];
    }
}