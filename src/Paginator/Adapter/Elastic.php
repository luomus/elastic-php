<?php
namespace Elastic\Paginator\Adapter;

use Elastic\Client;
use Elastic\Query\Query;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\ResultSet\ResultSetInterface;
use Zend\Paginator\Adapter\AdapterInterface;

class Elastic implements AdapterInterface
{

    protected $query;
    protected $search;
    protected $resultSetPrototype;
    protected $count = null;

    public function __construct(Query $query, Client\Search $search, ResultSetInterface $resultSet = null)
    {
        $this->query = $query;
        $this->search = $search;
        $this->resultSetPrototype = $resultSet ?: new ResultSet();
    }

    /**
     * Returns a collection of items for a page.
     *
     * @param  int $offset Page offset
     * @param  int $itemCountPerPage Number of items per page
     * @return array
     * @throws \Exception
     */
    public function getItems($offset, $itemCountPerPage)
    {
        $results = $this->getResults($offset, $itemCountPerPage);
        $errors = $results->getError();
        if (!empty($errors)) {
            throw new \Exception($errors);
        }
        $this->count = $results->count();

        $resultSet = clone $this->resultSetPrototype;
        $resultSet->initialize($results);

        return $resultSet;
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        if ($this->count === null) {
            $results = $this->getResults();
            $this->count = $results->count();
        }
        return $this->count;
    }

    private function getResults($offset = 0, $itemCountPerPage = 0)
    {
        $query = clone $this->query;
        $query->setFrom($offset);
        $query->setSize($itemCountPerPage);

        return $this->search->query($query)->execute();
    }
}