<?php
return array(
    'service_manager' => array(
        'invokables' => array(
            'Elastic\Service\AutocompleteService' => 'Elastic\Service\AutocompleteService'
        ),
        'factories' => array(
            'Elastic\Options\ModuleConfiguration' => 'Elastic\Factory\ModuleConfigurationFactory',
            'Elastic\Client\Index' => 'Elastic\Factory\IndexFactory',
            'Elastic\Client\Search' => 'Elastic\Factory\SearchFactory'
        ),
    ),
);